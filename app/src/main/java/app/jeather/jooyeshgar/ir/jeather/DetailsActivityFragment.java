package app.jeather.jooyeshgar.ir.jeather;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A placeholder fragment containing a simple view.
 */
public class DetailsActivityFragment extends Fragment {

    public DetailsActivityFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        String data = getActivity().getIntent().getStringExtra(MainFragment.EXTRA_NAME);

        View rootView = inflater.inflate(R.layout.fragment_details, container, false);
        TextView textView = (TextView) rootView.findViewById(R.id.data);
        textView.setText(data);
        return rootView;
    }
}
